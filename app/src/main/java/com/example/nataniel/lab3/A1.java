package com.example.nataniel.lab3;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.View;

public class A1 extends AppCompatActivity implements SensorEventListener2
{
    private final float GRAVITY = 9.8f;             // acceleration of gravity
    private final float FACTOR_FRICTION = 0.7f;     // imaginary friction on the screen
    private final float FACTOR_BOUNCEBACK = 0.8f;   //  bounce back force modifier
    final int ANDROID_STUDIO_HEADER_SIZE = 150;     //  need to be accounted for on the Y axis
    final int BALL_SIZE = 100;                      //  size of the ball. ball rendered in upper left corner, need offset on the max sides for collision detection
    final int RECTANGLE_BORDER = 20;                //  border in all directions
    final int VIBRATE_DURATION = 50;                //  duration of vibration
    final int SOUND_VOLUME = 100;                   //  volume of sound effect

    private float xPosition = 0.0f;
    private float xVelocity = 0.0f;
    private float yPosition = 0.0f;
    private float yVelocity = 0.0f;
    float xRotation = 0.0f;
    float yRotation = 0.0f;
    private float displayXMax = 0.0f;
    private float displayYMax = 0.0f;
    private float rectangleXMax = 0.0f;
    private float rectangleXMin = 0.0f;
    private float rectangleYMax = 0.0f;
    private float rectangleYMin = 0.0f;

    private SensorManager sensorManager = null;
    private Bitmap ball = null;
    private Vibrator haptic = null;
    private ToneGenerator sound = null;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);                                          //lock to landscape mode
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);                                                          //remove title
        getWindow().setFlags(0xFFFFFFFF,LayoutParams.FLAG_FULLSCREEN|LayoutParams.FLAG_KEEP_SCREEN_ON);   //fullscreen
        BallView ballView = new BallView(this);
        setContentView(ballView);

        Point size = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size);
        displayXMax = (float) size.x;
        displayYMax = (float) size.y - ANDROID_STUDIO_HEADER_SIZE;
        xPosition = (displayXMax - BALL_SIZE) / 2;
        yPosition = (displayYMax - BALL_SIZE) / 2;

        rectangleXMax = displayXMax - RECTANGLE_BORDER;
        rectangleXMin = RECTANGLE_BORDER;
        rectangleYMax = displayYMax - RECTANGLE_BORDER;
        rectangleYMin = RECTANGLE_BORDER;

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        haptic = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        sound = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, SOUND_VOLUME);
    }


    //
    @Override
    protected void onStart()
    {
        super.onStart();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_GAME);
    }

    //unregister listener BEFORE system frees up resources
    @Override
    protected void onStop() {
        sensorManager.unregisterListener(this);
        super.onStop();
    }

    //needed to be included for SensorEventListener2
    @Override
    public void onFlushCompleted(Sensor sensor)
    {

    }


    @Override
    public void onSensorChanged (SensorEvent sensorEvent)
    {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE)
        {
            if (Math.abs(sensorEvent.values[0]) > 0.02)
            {
                xRotation += sensorEvent.values[0];
            }

            if (Math.abs(sensorEvent.values[1]) > 0.02)
            {
                yRotation += sensorEvent.values[1];
            }

            //acceleration math from tutorial for friction, does not work with the rotation upset.
            //xAcceleration = Math.signum(xAcceleration) * Math.abs(xAcceleration) * (1 - FACTOR_FRICTION * Math.abs(xAcceleration) / GRAVITY);
            //yAcceleration = Math.signum(yAcceleration) * Math.abs(yAcceleration) * (1 - FACTOR_FRICTION * Math.abs(yAcceleration) / GRAVITY);
            updateBall();
        }
    }

    private void collisionEvent()
    {
        //TODO add timestamp to controll how often sound and vibrate can be triggered
        haptic.vibrate(VIBRATE_DURATION);
        sound.startTone(ToneGenerator.TONE_PROP_BEEP);
    }

    private void updateBall()
    {
        //TODO add friction to ball movement
        float frameTime = 0.8f; // imaginary time interval between each acceleration updates/ placeholder for delta time

        xVelocity += (xRotation/20 * frameTime);
        yVelocity += (yRotation/20 * frameTime);

        xPosition += xVelocity * frameTime;
        yPosition -= yVelocity * frameTime;

        //X position boundaries
        if (xPosition > (rectangleXMax - BALL_SIZE))
        {
            collisionEvent();
            xPosition = (rectangleXMax - BALL_SIZE);
            xVelocity = -xVelocity * FACTOR_BOUNCEBACK;
            //System.out.println("xVelocity is -:" + xVelocity);
        }
        else if (xPosition < rectangleXMin)
        {
            collisionEvent();
            xPosition = rectangleXMin;
            xVelocity = -xVelocity * FACTOR_BOUNCEBACK;
            //System.out.println("xVelocity is +:" + xVelocity);
        }

        //Y position boundaries
        if (yPosition > (rectangleYMax - BALL_SIZE))
        {
            collisionEvent();
            yPosition = (rectangleYMax - BALL_SIZE);
            yVelocity = -yVelocity * FACTOR_BOUNCEBACK;
            //System.out.println("yVelocity is :" + yVelocity);
        }
        else if (yPosition < rectangleYMin)
        {
            collisionEvent();
            yPosition = rectangleYMin;
            yVelocity = -yVelocity * FACTOR_BOUNCEBACK;
            //System.out.println("yVelocity is :" + yVelocity);
        }
    }

    //needed to be included for SensorEventListener2
    @Override
    public void onAccuracyChanged(Sensor sensor, int i)
    {

    }

    private class BallView extends View
    {
        public BallView(Context context)
        {
            super(context);
            Bitmap ballSrc = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
            final int dsWidth = BALL_SIZE;
            final int dsHeight = BALL_SIZE;
            ball = Bitmap.createScaledBitmap(ballSrc, dsWidth, dsHeight, true);

        }
        @Override
        protected void onDraw (Canvas canvas)
        {
            {
                Paint colourBlack = new Paint();
                colourBlack.setColor(Color.rgb(0, 0, 0));
                colourBlack.setStrokeWidth(10);
                canvas.drawRect(rectangleXMin, rectangleYMax, rectangleXMax, rectangleYMin, colourBlack);  //left, bottom, right, top
                canvas.drawBitmap (ball, xPosition, yPosition, null);
                invalidate();
            }
        }
    }
}
