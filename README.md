# lab 3 #

Done by: Nataniel Gåsøy


### Objective: Move a ball on the screen with sensors (gyroscope) ###

Create an app consisting of a single activity, that only works in a single mode: landscape. The activity will have a black rectangle drawn on a white background with a small margin on the outside of the square. 
Draw a circle, or render an image, that is roughly 5% of the width of the screen. The circle (or image) will move around and bounce off the boundery.
The movement will be controlled by the phone sensors (gyroscope, accelerometer, gravity vector). The task is to detect the tilt of the phone, and move the "ball" towards the lowest point of the phone tilt.
The app will make the phone emit a "ping" sound, and the phone will vibrate just enough to signal the ball bouncing off the boundary. 